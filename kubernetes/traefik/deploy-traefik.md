# Deploy Traefik in Google Kubernetes Engine

#### 1 - Get value of password field
- gcloud container clusters describe CUSTER_NAME --zone ZONE_NAME | grep password

#### 2 - Configure RBAC
- kubectl apply -f https://raw.githubusercontent.com/containous/traefik/master/examples/k8s/traefik-rbac.yaml  --username=admin --password=PASSWORD

#### 3 - Deploy Traefik Ingress Service with NodePort
- kubectl apply -f https://raw.githubusercontent.com/containous/traefik/master/examples/k8s/traefik-deployment.yaml  --username=admin --password=PASSWORD

#### 3 - Or Deploy Traefik Ingress Service with LoadBalancer
- kubectl apply -f https://gist.githubusercontent.com/georgepaoli/969b3e7e468ad581b8d9420f82202927/raw/e1f016fe894333894f51530463d0ab0b02ad2961/traefik-deployment.yaml --username=admin --password=PASSWORD

Open traefik web dashboard..


### Tests

#### Deploy NGINX
- kubectl apply -f https://gist.githubusercontent.com/georgepaoli/969b3e7e468ad581b8d9420f82202927/raw/ab1ba7afd4b1ec445eddc70c242d35269fb3086f/deploy-nginx.yaml

#### Deploy WHOAMI
- kubectl apply -f https://gist.githubusercontent.com/georgepaoli/969b3e7e468ad581b8d9420f82202927/raw/5824057de34833a577857e90afa4f0449edf5947/deploy-whoami.yaml