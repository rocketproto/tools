#!/bin/bash

echo "Installing Node18 and npm"
curl -fsSL https://deb.nodesource.com/setup_18.x | sudo -E bash -
sudo apt-get install -y nodejs

echo "Node version: "
node --version

echo "NPM version: "
npm --version

npm install -g @angular/cli

echo "ng version: "
ng --version