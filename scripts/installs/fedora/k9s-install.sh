sudo echo "Downloading"
curl -L -o k9s_Linux_amd64.tar.gz https://github.com/derailed/k9s/releases/download/v0.27.2/k9s_Linux_amd64.tar.gz 

echo "Extracting" 
tar -xf k9s_Linux_amd64.tar.gz

echo "Installing"
sudo mv k9s /usr/bin/
rm -f k9s_*
rm LICENSE
rm README.md




