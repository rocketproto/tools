# Linux Shortcuts
alias ls='ls -FA'
alias ll='ls -lhFA'
alias sb='echo "source ~/.bashrc" && source ~/.bashrc'
alias hs='history | grep'

# Kubernetes
alias k='kubectl'
alias ka='kubectl apply'
alias kd='kubectl delete'

# Networking
alias ports='sudo ss -tulpn | grep LISTEN'
alias host='hostname -I'

# Unix
alias OS='hostnamectl'
alias kernal='uname -r'


