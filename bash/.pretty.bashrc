

parse_git_branch() {
     git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}

export BLACK="\e[30m"
export RED="\e[31m"   #"\[\033[31m\]"
export GREEN="\e[32m"
export YELLOW="\e[33m"
export BLUE="\e[34m"
export MAGENTA="\e[35m"
export CYAN="\e[36m"
export NOCOLOUR="\e[0m"

export USER="\u"
export HOST="\h "
export NEWLINE="\n"
export WORKDIR="\w"

export GIT_BRANCH_NAME="\$(parse_git_branch) "
export TIME="[\$(date +'%H:%M')]:"

# export PS1="\u@\h \[\033[32m\]\w\[\033[33m\]\$(parse_git_branch)\[\033[00m\] $ "

export PS1=""

export PS1=$PS1$MAGENTA
export PS1=$PS1$USER@$HOST
export PS1=$PS1$GREEN
export PS1=$PS1$WORKDIR
export PS1=$PS1$YELLOW
export PS1=$PS1$GIT_BRANCH_NAME
export PS1=$PS1$NOCOLOUR
export PS1=$PS1$TIME
export PS1=$PS1$NEWLINE
export PS1=$PS1"$ "