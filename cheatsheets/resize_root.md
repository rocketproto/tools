# List Disks
```
lsblk
```

# Resize Partition
```
sudo lvextend -L +900G /dev/mapper/fedora_linux-root
```

# Allocate Root
```
sudo xfs_growfs /dev/mapper/fedora_linux-root
```
